/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bestza012.stroreproject.poc;

import database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author WIN10
 */
public class TestUpdateProduct {
    public static void main(String[] args) {
        Connection conn = null;
        PreparedStatement stmt =null;
        // connect database
            Database db = Database.getInstance();
            conn= db.getConnection();
            //process
            try{
            String sql = "UPDATE product SET name = ?,price =? WHERE id = ?";
            stmt=conn.prepareStatement(sql);
            
            stmt.setString(1, "BESTKUNG");
            stmt.setDouble(2, 20);
            stmt.setInt(3, 6);
             stmt.executeUpdate();
      
            stmt.close();
            conn.close();
        
        } catch (SQLException ex) {
            System.out.println("Database connection failed!!!");
        }
        
    }
}
