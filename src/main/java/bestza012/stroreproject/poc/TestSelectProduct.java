/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bestza012.stroreproject.poc;

import database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Product;

/**
 *
 * @author WIN10
 */
public class TestSelectProduct {
   public static void main(String[] args) {
        Connection conn = null;
        Statement stmt =null;
        // connect database
        Database db = Database.getInstance();
        conn=db.getConnection();
        //process
        try {
            stmt = conn.createStatement();
            String sql = "SELECT * FROM product";
            ResultSet result = stmt.executeQuery(sql);
            
            while(result.next()){
                
                  int id = result.getInt("id");
                  String name = result.getString("name");
                  double price = result.getDouble("price");
                  Product product= new Product(id,name,price);
                 System.out.println(product);
            }
            
            result.close();
            stmt.close();
            db.close();
        } catch (SQLException ex) {
            System.out.println("Database connection failed!!!");
        }
        
        
    }
}
