/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bestza012.stroreproject.poc;

import database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import model.Product;

/**
 *
 * @author WIN10
 */
public class TestInsertProduct {
    public static void main(String[] args) {
        Connection conn = null;
        PreparedStatement stmt =null;
        String dbPath = "./db/storedb.db";
        // connect database
        Database db = Database.getInstance();
        conn=db.getConnection();
            //process
            try{
            String sql = "INSERT INTO product (name,price )VALUES (?,?)";
            stmt=conn.prepareStatement(sql);
            Product product = new Product(-1,"Oh Leing",20);
            stmt.setString(1, product.getName());
            stmt.setDouble(2, product.getPrice());
             int row = stmt.executeUpdate();
             // print last index was generated
             ResultSet result = stmt.getGeneratedKeys();
             int id=-1;
             if(result.next()){
                 id = result.getInt(1);
             }
            System.out.println("affect row : "+row+" id:"+id);
            stmt.close();
            conn.close();
        
        } catch (SQLException ex) {
            System.out.println("Database connection failed!!!");
        }
        
    }
}
