/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Seller;

/**
 *
 * @author WIN10
 */
public class SellerDao implements DaoIntefect<Seller> {

    @Override
    public int add(Seller object) {
        Database database = Database.getInstance();
        Connection conn = database.getConnection();
        int id = 0;
        String sql = "INSERT INTO seller (name, tel,password) VALUES (?,?,?)";
        PreparedStatement stmt;
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getTel());
            stmt.setString(3, object.getPassword());
            stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }

            database.close();

        } catch (SQLException ex) {
            Logger.getLogger(SellerDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return id;
    }

    @Override
    public ArrayList<Seller> getAll() {
        Database db = Database.getInstance();
        Connection conn = db.getConnection();
        ArrayList<Seller> list = new ArrayList<>();
        Statement stmt = null;
        //process
        String sql = "SELECT id,name,tel,password FROM seller";
        try {
            stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);

            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");
                String password = result.getString("password");
                Seller seller = new Seller(id, name, tel, password);
                list.add(seller);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SellerDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return list;
    }

    @Override
    public Seller get(int id) {
       Database db = Database.getInstance();
       Connection conn = db.getConnection();
       Seller seller = null;
       PreparedStatement stmt ;
       ResultSet result=null;
        try {
            stmt = conn.prepareStatement("SELECT id, name, tel, password FROM seller WHERE id = ?");
            stmt.setInt(1, id);
            
            result = stmt.executeQuery();
            
            if(result.next()){
                int pid = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");
                String password = result.getString("password");
                seller = new Seller(pid,name,tel,password);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(SellerDao.class.getName()).log(Level.SEVERE, null, ex);
        }
       db.close();
       return seller;
    }

    @Override
    public int delete(int id) {
        Database db = Database.getInstance();
        Connection conn = db.getConnection();
        Statement stmt=null;
        int row=0;
        String sql = "DELETE FROM seller WHERE id ="+id;
        
        
        try {
            stmt = conn.createStatement();
            row =stmt.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(SellerDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        db.close();
        return row;
    }

    @Override
    public int update(Seller object) {
       Database db = Database.getInstance();
       Connection conn = db.getConnection();
       int row=0;
        try {
            PreparedStatement stmt = conn.prepareStatement("UPDATE seller SET name = ?, tel = ?,  password = ? WHERE id = ?");
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getTel());
            stmt.setString(3, object.getPassword());
            stmt.setInt(4, object.getId());
            row=stmt.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(SellerDao.class.getName()).log(Level.SEVERE, null, ex);
        }
       
       
       db.close();
       return row;
    }

    public static void main(String[] args) {
        SellerDao seller = new SellerDao();
        int id=seller.add(new Seller(-1, "TAUNG", "09191", "HOMERUN01"));
        Seller lastSeller=seller.get(id);
        lastSeller.setName("PAKAWATTOO");
        seller.update(lastSeller);
        Seller updatesELLER = seller.get(id);
        System.out.println(updatesELLER);
    
    }
}
