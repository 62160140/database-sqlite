/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Product;

/**
 *
 * @author WIN10
 */
public class ProductDao implements DaoIntefect<Product> {

    @Override
    public int add(Product object) {
        Connection conn = null;
        PreparedStatement stmt = null;
        int id = -1;
        // connect database
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process
        try {
            String sql = "INSERT INTO product (name,price )VALUES (?,?)";
            stmt = conn.prepareStatement(sql);

            stmt.setString(1, object.getName());
            stmt.setDouble(2, object.getPrice());
            int row = stmt.executeUpdate();
            // print last index was generated
            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);
            }

            db.close();
        } catch (SQLException ex) {
            System.out.println("Database connection failed!!!");
        }
        return id;
    }

    @Override
    public ArrayList<Product> getAll() {
        ArrayList list = new ArrayList<>();
        Connection conn = null;
        Statement stmt = null;
        // connect database
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process
        try {
            stmt = conn.createStatement();
            String sql = "SELECT * FROM product";
            ResultSet result = stmt.executeQuery(sql);

            while (result.next()) {

                int id = result.getInt("id");
                String name = result.getString("name");
                double price = result.getDouble("price");
                Product product = new Product(id, name, price);
                list.add(product);
            }

            result.close();
            stmt.close();
            db.close();
        } catch (SQLException ex) {
            System.out.println("Database connection failed!!!");
        }

        return list;
    }

    @Override
    public Product get(int id) {
        Connection conn = null;
        Statement stmt = null;
        // connect database
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process
        try {
            stmt = conn.createStatement();
            String sql = "SELECT * FROM product WHERE id =" + id;
            ResultSet result = stmt.executeQuery(sql);

            while (result.next()) {

                int pid = result.getInt("id");
                String name = result.getString("name");
                double price = result.getDouble("price");
                Product product = new Product(pid, name, price);
                return product;

            }

            result.close();
            stmt.close();
            db.close();
        } catch (SQLException ex) {
            System.out.println("Database connection failed!!!");
        }
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        PreparedStatement stmt = null;
        int row = 0;
        // connect database
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            //process
            String sql = "DELETE FROM product WHERE id = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();

            db.close();
        } catch (SQLException ex) {
            System.out.println("Database connection failed!!!");
        }

        return row;
    }

    @Override
    public int update(Product object) {
        Connection conn = null;
        PreparedStatement stmt = null;
        int row = 0;
        // connect database
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process
        try {
            String sql = "UPDATE product SET name = ?,price =? WHERE id = ?";
            stmt = conn.prepareStatement(sql);

            stmt.setString(1, object.getName());
            stmt.setDouble(2, object.getPrice());
            stmt.setInt(3, object.getId());
            row = stmt.executeUpdate();

            db.close();

        } catch (SQLException ex) {
            System.out.println("Database connection failed!!!");
        }
        
        return row;

    }

    public static void main(String[] args) {
        ProductDao dao = new ProductDao();
        int id = dao.add(new Product(-1,"Latae",50));
        System.out.println(dao.get(id));
//        System.out.println(id);       
        //update
        Product lastProduct = dao.get(id);
        lastProduct.setPrice(100);
        dao.update(lastProduct);
        Product updateProduct = dao.get(id);
        System.out.println(updateProduct);
    //     System.out.println("updateproduct:"+dao.get(id));
    }   

}
