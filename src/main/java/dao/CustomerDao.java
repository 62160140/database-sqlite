/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Customer;
import model.Product;

/**
 *
 * @author WIN10
 */
public class CustomerDao implements DaoIntefect<Customer>{

    @Override
    public int add(Customer object) {
      Connection conn = null;
        PreparedStatement stmt = null;
        int id = -1;
        // connect database
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process
        try {
            String sql = "INSERT INTO customer (name,tel) VALUES (?,?)";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getTel());
            int row = stmt.executeUpdate();
            // print last index was generated
            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);
            }

            db.close();
        } catch (SQLException ex) {
            System.out.println("Database connection failed!!!");
        }
        return id;
    }

    @Override
    public ArrayList<Customer> getAll() {
        ArrayList list = new ArrayList<>();
        Connection conn = null;
        Statement stmt = null;
        // connect database
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process
        try {
            stmt = conn.createStatement();
            String sql = "SELECT * FROM customer";
            ResultSet result = stmt.executeQuery(sql);

            while (result.next()) {

                int id = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");
                Customer product = new Customer(id, name, tel);
                list.add(product);
            }

            result.close();
            stmt.close();
            db.close();
        } catch (SQLException ex) {
            System.out.println("Database connection failed!!!");
        }

        return list;
    }

    @Override
    public Customer get(int id) {
        Connection conn = null;
        Statement stmt = null;
        // connect database
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process
        try {
            stmt = conn.createStatement();
            String sql = "SELECT * FROM customer WHERE id =" + id;
            ResultSet result = stmt.executeQuery(sql);

            if(result.next()) {

                int pid = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");
                Customer customer = new Customer(pid, name, tel);
                return customer;

            }

            result.close();
            stmt.close();
            db.close();
        } catch (SQLException ex) {
            System.out.println("Database connection failed!!!");
        }
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        PreparedStatement stmt = null;
        int row = 0;
        // connect database
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            //process
            String sql = "DELETE FROM customer WHERE id = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();

            db.close();
        } catch (SQLException ex) {
            System.out.println("Database connection failed!!!");
        }

        return row;
    }


    @Override
    public int update(Customer object) {
       Connection conn = null;
        PreparedStatement stmt = null;
        int row = 0;
        // connect database
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process
        try {
            String sql = "UPDATE customer SET name = ?,tel =? WHERE id = ?";
            stmt = conn.prepareStatement(sql);

            stmt.setString(1, object.getName());
            stmt.setString(2, object.getTel());
            stmt.setInt(3, object.getId());
            row = stmt.executeUpdate();

            db.close();

        } catch (SQLException ex) {
            System.out.println("Database connection failed!!!");
        }
        
        return row;
    }
    
    
    public static void main(String[] args) {
        CustomerDao dao = new CustomerDao();
        System.out.println(dao.getAll());
        
        int inx = dao.add(new Customer(-1,"PAKAWAT","0990097351"));
        Customer lastCustomer = dao.get(inx);
        lastCustomer.setTel("0865678926");
        dao.update(lastCustomer);
        Customer updateCustomer = dao.get(inx);
        System.out.println(updateCustomer);
        
//        System.out.println(dao.delete(1));
    }
    
}
